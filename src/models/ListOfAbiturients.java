package src.models;

import java.util.Arrays;

public class ListOfAbiturients {
    private Abiturient[] abiturients = new Abiturient[10];

    public Abiturient[] getAbiturients() {
        return abiturients;
    }

    @Override
    public String toString() {
        return "Список абитуриентов" + Arrays.toString(abiturients) +
                '}';
    }
}
