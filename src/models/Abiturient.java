package src.models;

import src.Service.AbiturientService;
import src.Service.IAbiturientService;

import java.util.Arrays;

public class Abiturient implements Comparable<Abiturient> {
    private String name;
    private String surname;
    private int[] marks;
    private double avgMark;
    private IAbiturientService abiturientService;

    public Abiturient(String name, String surname, int[] marks) {
        this.name = name;
        this.surname = surname;
        this.marks = marks;
        abiturientService = new AbiturientService();
        setAvgMark(abiturientService.calculateAvgMark(marks));
    }


    private void setAvgMark(double avgMark) {
        this.avgMark = avgMark;
    }

    @Override
    public String toString() {
        return
                "\n" + "Имя - " + name  + " " + surname + "."  +
                " Оценки - " + Arrays.toString(marks) +
                ". Средняя оценка - " + avgMark ;
    }

    public int compareTo(Abiturient input) {
        return Double.compare(input.avgMark, this.avgMark);
    }
}


