package src.Service;

import src.models.Abiturient;
import src.models.ListOfAbiturients;

public interface IAbiturientService {

    boolean addAbiturientToTheList(ListOfAbiturients listOfAbiturients, Abiturient abiturient);
    double calculateAvgMark(int[] marks);
    Abiturient[] abiturientsWhoEntered (ListOfAbiturients listOfAbiturients, int numberOfStudents);

}
