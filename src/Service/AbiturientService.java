package src.Service;

import src.models.Abiturient;
import src.models.ListOfAbiturients;

import java.util.Arrays;


public class AbiturientService implements IAbiturientService {

    @Override
    public boolean addAbiturientToTheList(ListOfAbiturients listOfAbiturients, Abiturient abiturient) {
        for (int i = 0; i < listOfAbiturients.getAbiturients().length; i++) {
            if (listOfAbiturients.getAbiturients()[i] == null) {
                listOfAbiturients.getAbiturients()[i] = abiturient;
                return true;
            }
        }
        return false;
    }


    @Override
    public double calculateAvgMark(int[] marks) {
        int sum = 0;
        for(int mark : marks) {
            sum += mark;
        }
        return (double) sum / marks.length;
    }

    @Override
    public Abiturient[] abiturientsWhoEntered (ListOfAbiturients listOfAbiturients, int numberOfStudents) {
        if (listOfAbiturients.getAbiturients().length < numberOfStudents) {
            return listOfAbiturients.getAbiturients();
        }
        else {
            Arrays.sort(listOfAbiturients.getAbiturients());
            return Arrays.copyOf(listOfAbiturients.getAbiturients(), numberOfStudents);
        }
    }
}


