package src.Demo;

import src.Service.AbiturientService;
import src.Service.IAbiturientService;
import src.models.Abiturient;
import src.models.ListOfAbiturients;

import java.util.Arrays;

public class DemoService implements IDemoService {



    @Override
    public void execute() {
        ListOfAbiturients listOfAbiturients = new ListOfAbiturients();
        IAbiturientService abiturientService = new AbiturientService();


       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Kirill", "Ostanin", new int[] {5,4,5,5,5}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Nikolai", "Kasyanov", new int[] {3,4,4,5,4}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Maksim", "Gavrilov", new int[] {5,5,5,5,3}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Alex", "Sergeev", new int[] {4,4,4,5,4}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Mihail", "Dementev", new int[] {3,2,5,5,4}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Roman", "Maksimov", new int[] {5,5,5,5,5}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Anton", "Smirnov", new int[] {3,3,3,5,4}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Daria", "Bokun", new int[] {4,4,4,5,5}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Mihail", "Chegarov", new int[] {2,2,2,5,4}) );
       abiturientService.addAbiturientToTheList(listOfAbiturients, new Abiturient("Denis", "Shiryaev", new int[] {4,4,3,3,3}) );

       System.out.println("Список поступивших: ");
       System.out.println(Arrays.toString(abiturientService.abiturientsWhoEntered(listOfAbiturients, 6)));

    }

}
